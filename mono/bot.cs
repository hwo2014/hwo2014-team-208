using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Xml;
using Newtonsoft.Json;

public class Bot
{
  public static void Main(string[] args)
  {
    string host = args[0];
    int port = int.Parse(args[1]);
    string botName = args[2];
    string botKey = args[3];
    string color = "red";
    int max = 0;

    if (args.Length > 4)
      color = args[4];

    if (args.Length > 5)
      max = int.Parse(args[5]);

    HistoryData history = new HistoryData();

    Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);


    using (TcpClient client = new TcpClient(host, port))
    {
      NetworkStream stream = client.GetStream();
      StreamReader reader = new StreamReader(stream);
      StreamWriter writer = new StreamWriter(stream);
      writer.AutoFlush = true;

      new Bot(reader, writer, new Join(botName, botKey), history);
    }


    //history.turboActivated = false;
    //history.turboAvailable = false;
    //history.turboTicksLeft = 0;
    //history.turboFactor = 1;
    //history.SpeedAddedThisLap = 0;
    //history.lastAngleSpeed = 0;
    //history.lastPiece = 0;
    //history.lastPieceDistance = 0;
    //history.lastTick = 0;
    //history.lastThrotle = 1;
    //history.lastAngle = 0;
    //history.lastSpeed = 0;
    //history.bendMaxAngle = 0;
    //history.maxAngleGuess = 0;


    //using (TcpClient client = new TcpClient(host, port))
    //{
    //  NetworkStream stream = client.GetStream();
    //  StreamReader reader = new StreamReader(stream);
    //  StreamWriter writer = new StreamWriter(stream);
    //  writer.AutoFlush = true;

    //  new Bot(reader, writer, new Join(botName, botKey, color), history);
    //}
  }

  private StreamWriter writer;

  Bot(StreamReader reader, StreamWriter writer, Join join, HistoryData history)
  {
    this.writer = writer;

    //JoinRace raceJoin = new JoinRace("elaeintarha", "mapvision", 1, join);
    //JoinRace raceJoin = new JoinRace("imola", "mapvision", 1, join);
    //JoinRace raceJoin = new JoinRace("england", "mapvision", 1, join);
    // JoinRace raceJoin = new JoinRace("suzuka", "hey", 1, join);
    //JoinRace raceJoin = new JoinRace("germany", "mapvision", 1, join);
    //JoinRace raceJoin = new JoinRace("germany", "mapvision", 3, join);
    //JoinRace raceJoin = new JoinRace("keimola", "mapvision", 2, join);
    //JoinRace raceJoin = new JoinRace("keimola", "mapvision", 1, join);
    ////JoinRace raceJoin = new JoinRace("usa", "mapvision", 1, join);
    //JoinRace raceJoin = new JoinRace("usa", "mapvision", 2, join);
    //JoinRace raceJoin = new JoinRace("usa", "mapvision", 3, join);
    //JoinRace raceJoin = new JoinRace("france", "mapvision", 1, join);
    //JoinRace raceJoin = new JoinRace("france", "mapvision", 2, join);
    //JoinRace raceJoin = new JoinRace("france", "mapvision", 3, join);
    // send(raceJoin);
     
    send(join);
    run(reader, writer, join, history);
  }

  private void run(StreamReader reader, StreamWriter writer, Join join, HistoryData history)
  {
    this.writer = writer;
    string line;
    string botName = join.name;
    string color = "";
    Race race = null;

    double currentSpeed = 0;
    double maxSpeed = 0;
    CarIdentifier car = null;
    double brakeTestStartSpeed = 0;

    //history.brake = -0.082;
    history.brake = -0.062;

    while ((line = reader.ReadLine()) != null)
    {
      MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
      switch (msg.msgType)
      {
        case "carPositions":
          System.Collections.Generic.List<CarPosition> cars = JsonConvert.DeserializeObject<System.Collections.Generic.List<CarPosition>>(msg.data.ToString());
          string gameinfo = line;
          gameinfo = "{ " + gameinfo.Substring(gameinfo.IndexOf(']') + 2);
          GameInfo info = JsonConvert.DeserializeObject<GameInfo>(gameinfo);
          CarPosition myCar = null;
          double throtle = 0;

          int index = 0;
          while (index < cars.Count)
          {
            if (cars[index].id.name == botName)
            {
              myCar = cars[index];
              break;
            }
            index++;
          }

          index = 0;
          SortedList<double, CarPosition> nearCars = new SortedList<double, CarPosition>();
          double distanceToCar = 0;
          CarPosition nearestCar = null;

          while (index < cars.Count)
          {
            if (cars[index].id.color != myCar.id.color)
            {
              double tempDistanceToCar = race.track.TrackLengthToPiece(cars[index].piecePosition.pieceIndex, myCar.piecePosition.lane.startLaneIndex) + cars[index].piecePosition.inPieceDistance -
                  race.track.TrackLengthToPiece(myCar.piecePosition.pieceIndex, myCar.piecePosition.lane.startLaneIndex) - myCar.piecePosition.inPieceDistance;

              if (System.Math.Abs(tempDistanceToCar) > race.track.TrackLength(myCar.piecePosition.lane.startLaneIndex) / 2)
              {
                if (tempDistanceToCar < 0)
                  tempDistanceToCar += race.track.TrackLength(myCar.piecePosition.lane.startLaneIndex);
                if (tempDistanceToCar > 0)
                  tempDistanceToCar -= race.track.TrackLength(myCar.piecePosition.lane.startLaneIndex);
              }

              nearCars.Add(tempDistanceToCar, cars[index]);
              if (nearestCar == null)
              {
                distanceToCar = tempDistanceToCar;
                nearestCar = cars[index];
              }
              else
              {
                if (tempDistanceToCar < distanceToCar)
                {
                  distanceToCar = tempDistanceToCar;
                  nearestCar = cars[index];
                }
              }
            }

            if (race.track.pieces[cars[index].piecePosition.pieceIndex].@switch && race.track.lastProcessedSwitch[cars[index].id.color] != cars[index].piecePosition.pieceIndex)
            {
              //previous switch
              int indexOfPreviousSwitch = race.track.indexOfPreviousSwitch(cars[index].piecePosition.pieceIndex);
              double distanceFromPrevious = race.track.DistanceToPiece(indexOfPreviousSwitch, cars[index].piecePosition.pieceIndex, cars[index].piecePosition.lane.startLaneIndex);
              int ticksSinceLastSwitch = info.gameTick - history.laneDecissions[cars[index].id.color][indexOfPreviousSwitch].startTick;
              double speed = distanceFromPrevious / ticksSinceLastSwitch;
              //Console.WriteLine(indexOfPreviousSwitch + " " + cars[index].piecePosition.pieceIndex + " " + distanceFromPrevious + " " + ticksSinceLastSwitch + " " + speed + " " + history.laneDecissions[cars[index].id.color][indexOfPreviousSwitch].startTick + " " + info.gameTick);
              history.laneDecissions[cars[index].id.color][indexOfPreviousSwitch].addSpeedObservation(speed);

              //next switch 
              history.laneDecissions[cars[index].id.color][cars[index].piecePosition.pieceIndex].addLaneObservation(cars[index].piecePosition.lane.endLaneIndex);
              history.laneDecissions[cars[index].id.color][cars[index].piecePosition.pieceIndex].startTick = info.gameTick;

              race.track.lastProcessedSwitch[cars[index].id.color] = cars[index].piecePosition.pieceIndex;
            }

            index++;
          }

          currentSpeed = this.calculateSpeed(race.track, myCar, info.gameTick, history);

          if (distanceToCar > 0 && distanceToCar < race.cars[0].dimensions.length + 10)
            race.track.doingTrick = false;

          if (currentSpeed > maxSpeed)
            maxSpeed = currentSpeed;

          if (myCar != null)
          {
            if (!history.initialised && info.gameTick < 40)
            {
              brakeTestStartSpeed = currentSpeed;
              send(new Throttle(1, info.gameTick));
            }
            else if (!history.initialised && info.gameTick < 41)
            {
              history.c = (2 * (history.lastSpeed - currentSpeed) / (history.lastSpeed * history.lastSpeed));
              //Console.WriteLine("coe " + history.c);
              //Console.WriteLine("airDrag " + history.c * currentSpeed * currentSpeed);
              send(new Throttle(0, info.gameTick));
            }
            else if (!history.initialised && info.gameTick < 80)
            {
              history.brake = (currentSpeed - brakeTestStartSpeed) / (info.gameTick - 39);
              Console.WriteLine(history.brake);
              send(new Throttle(0, info.gameTick));
            }
            else if (!history.initialised && info.gameTick == 80)
            {
              history.brake = (currentSpeed - brakeTestStartSpeed) / 41;
              Console.WriteLine(history.brake);
              history.initialised = true;
              send(new Throttle(1, info.gameTick));
            }
            else
            {
              double distance = distanceToCar;
              //if (myCar.piecePosition.lane.endLaneIndex != nearestCar.piecePosition.lane.endLaneIndex)
              //  distance = 100;
              int nextPieceIndex = myCar.piecePosition.pieceIndex + 1;
              if (nextPieceIndex == race.track.pieces.Count)
                nextPieceIndex = 0;
 
              int @switch = 0;

              bool shouldDo = history.carToDodge != null;
              @switch = this.calculateSwitch(race.track.pieces[nextPieceIndex].@switch, race.track, myCar, history, nearCars, race.cars[0].dimensions.length);

              throtle = this.determineThrotle(race.track, myCar, currentSpeed, history, info.gameTick, race.raceSession.laps, distance, @switch);


              if ((race.track.pieces[nextPieceIndex].@switch || (shouldDo && history.carToDodge == null)) && @switch > 0 && !history.laneSwithced)
              {
                history.laneSwithced = true;
                //send(new Ping());
                send(new SwitchLane("Right", info.gameTick));
              }
              else if ((race.track.pieces[nextPieceIndex].@switch || (shouldDo && history.carToDodge == null)) && @switch < 0 && !history.laneSwithced)
              {
                history.laneSwithced = true;
                //send(new Ping());
                send(new SwitchLane("Left", info.gameTick));
              }
              else if (history.turboAvailable && race.track.LetsTurbo(myCar.piecePosition.pieceIndex, history.turboTicksLeft, myCar, nearestCar, race.raceSession.laps, distanceToCar))
              {
                send(new TurboActivate("oujeee!!!", info.gameTick));
                history.turboAvailable = false;
              }
              else
              {
                send(new Throttle(throtle, info.gameTick));
              }
            }

            history.lastDistances = nearCars;
            history.lastLane = myCar.piecePosition.lane.endLaneIndex;
            history.lastThrotle = throtle;
            history.lastAngle = myCar.angle;
            history.lastTick = info.gameTick;
            history.lastPiece = myCar.piecePosition.pieceIndex;
            history.lastSpeed = currentSpeed;
            history.lastLap = myCar.piecePosition.lap;
            history.lastPieceDistance = myCar.piecePosition.inPieceDistance;
          }
          else
          {
            send(new Ping());
          }
          break;
        case "join":
          Console.WriteLine("Joined");
          //send(new Ping());
          break;
        case "gameInit":
          Console.WriteLine("Race init");
          race = JsonConvert.DeserializeObject<RaceInit>(msg.data.ToString()).race;
          race.init();

          history.laneDecissions.Clear();

          SortedList<int, LaneDecission> laneDecissions = race.track.GetLaneDecissions();
          foreach (Car someCar in race.cars)
          {
            if (!history.laneDecissions.Keys.Contains(someCar.id.color))
            {
              history.laneDecissions.Add(someCar.id.color, new SortedList<int, LaneDecission>());

              foreach (int key in laneDecissions.Keys)
              {
                history.laneDecissions[someCar.id.color].Add(key, new LaneDecission(laneDecissions[key]));
              }
            }

            race.track.lastProcessedSwitch.Add(someCar.id.color, 0);
          }

          race.track.SetForce(history.lastRaceTargetForce);

          //send(new Ping());
          break;
        case "gameEnd":
          Console.WriteLine("Race ended");
          Console.WriteLine("Max speed: " + maxSpeed);
          history.lastRaceTargetForce = race.track.pieces[0].targetSpeed;
          Console.WriteLine("Target: " + history.lastRaceTargetForce);
          foreach (string key in history.laneDecissions.Keys)
          {
            Console.WriteLine("Car: " + key);
            foreach (LaneDecission lane in history.laneDecissions[key].Values)
            {
              Console.WriteLine("Lane decission: " + lane.startIndex + " " + lane.getAverageLane() + " Average: " + lane.getAverageSpeed());
            }
          }
          //send(new Ping());
          break;
        case "gameStart":
          Console.WriteLine("Race starts");
          Throttle throtle2 = JsonConvert.DeserializeObject<Throttle>(line);
          send(new Throttle(1, throtle2.gameTick));
          break;
        case "yourCar":
          Console.WriteLine("Your Car");
          car = JsonConvert.DeserializeObject<CarIdentifier>(msg.data.ToString());
          botName = car.name;
          color = car.color;
          //send(new Ping());
          break;
        case "crash":
          car = JsonConvert.DeserializeObject<CarIdentifier>(msg.data.ToString());
          Console.WriteLine("Crash " + car.name);
          if (car.color == color && car.name == botName && history.initialised)
          {
            race.track.maxForce = race.track.pieces[history.lastPiece].targetSpeed - 0.02;
            race.track.SetForce(race.track.pieces[history.lastPiece].targetSpeed - 0.03);
            Console.WriteLine("Crash " + race.track.pieces[history.lastPiece].targetSpeed);

          }
          //send(new Throttle(1));
          break;
        case "spawn":
          car = JsonConvert.DeserializeObject<CarIdentifier>(msg.data.ToString());
          Console.WriteLine("Spawn " + car.name);

          //send(new Throttle(1));
          break;
        case "turboAvailable":
          Turbo turbo = JsonConvert.DeserializeObject<Turbo>(msg.data.ToString());
          Console.WriteLine("Turbo Available for " + turbo.turboDurationTicks);
          history.turboFactor = turbo.turboFactor;
          history.turboTicksLeft = turbo.turboDurationTicks;
          history.turboAvailable = true;
          break;
        case "turboStart":
          car = JsonConvert.DeserializeObject<CarIdentifier>(msg.data.ToString());
          if (car.color == color && car.name == botName)
          {
            history.turboActivated = true;
            history.turboAvailable = false;
            Console.WriteLine("Turbo Activated");
          }
          else
          {
            foreach (double distance in history.lastDistances.Keys)
            {
              if (car.color == history.lastDistances[distance].id.color && distance > -150 && distance < 0)
                history.carToDodge = history.lastDistances[distance];
            }
          }
          break;
        case "turboEnd":
          car = JsonConvert.DeserializeObject<CarIdentifier>(msg.data.ToString());
          if (car.color == color && car.name == botName)
          {
            history.turboActivated = false;
            history.turboFactor = 1;
            Console.WriteLine("Turbo Ended");
          }
          break;
        default:
          Console.WriteLine(msg.msgType.ToString());
          //send(new Ping());
          break;
      }
    }
  }

  private int calculateSwitch(bool nextIsSwitch, Track track, CarPosition myCar, HistoryData history, SortedList<double, CarPosition> cars, double length)
  {
    //int nextSwitchIndex = track.indexOfNextSwitch(myCar.piecePosition.pieceIndex
    int shorterLane = track.getShorterLane(myCar.piecePosition.pieceIndex, myCar.piecePosition.lane.startLaneIndex);
    int overtakeLane = -100;
    int indexOfNearCar = 0;
    if (history.carToDodge != null && !history.turboActivated)
    {
      int indexOfNextSwitch = track.indexOfNextSwitch(myCar.piecePosition.pieceIndex);
      double distanceToSwitch = track.DistanceToPiece(myCar.piecePosition.pieceIndex, indexOfNextSwitch, myCar.piecePosition.lane.endLaneIndex);
      if (distanceToSwitch < 100)
      {
        if (shorterLane == 0)
        {
          if (myCar.piecePosition.lane.startLaneIndex == 0)
            overtakeLane = 1;
          else if (myCar.piecePosition.lane.startLaneIndex == track.lanes.Count - 1)
            overtakeLane = -1;
          else
            overtakeLane = -1;
        }
        else
        {
          overtakeLane = 0;
        }
      }

      Console.WriteLine("Should do something!!!");
      history.carToDodge = null;
    }
    else if (nextIsSwitch && cars.Count > 0)
    {
      while (indexOfNearCar < cars.Count)
      {
        if (cars.Keys[indexOfNearCar] > 0)
          break;

        indexOfNearCar++;
      }

      while (overtakeLane == -100 && indexOfNearCar < cars.Count)
      {
        double distanceToCar = cars.Keys[indexOfNearCar];
        CarPosition car = cars.Values[indexOfNearCar];
        double myAverageSpeed = history.laneDecissions[myCar.id.color][myCar.piecePosition.pieceIndex + 1].getAverageSpeed();
        double otherAverageSpeed = history.laneDecissions[car.id.color][myCar.piecePosition.pieceIndex + 1].getAverageSpeed();
        double differenceDistance = 0;
        bool averageSpeedsAvailable = false;
        if (myAverageSpeed != 0 && otherAverageSpeed != 0)
        {
          double distance = track.DistanceToPiece(myCar.piecePosition.pieceIndex + 1, track.indexOfNextSwitch(myCar.piecePosition.pieceIndex + 1), myCar.piecePosition.lane.startLaneIndex + shorterLane);
          double myTime = distance / myAverageSpeed;
          double otherTime = distance / otherAverageSpeed;

          differenceDistance = (otherTime - myTime) * myAverageSpeed;
          averageSpeedsAvailable = true;
        }

        int temp = (int)(history.laneDecissions[car.id.color][myCar.piecePosition.pieceIndex + 1].getAverageLane() + 0.5);
        Console.WriteLine(shorterLane + " " + distanceToCar + " " + temp);
        if ((int)(history.laneDecissions[car.id.color][myCar.piecePosition.pieceIndex + 1].getAverageLane() + .5) == (myCar.piecePosition.lane.startLaneIndex + shorterLane) &&
          ((averageSpeedsAvailable && distanceToCar > 0 && differenceDistance > 0 && differenceDistance > (distanceToCar - 4 * length)) ||
          (!averageSpeedsAvailable && distanceToCar > 0 && distanceToCar < 80)))//distanceToCar > 0 && distanceToCar < 80)
        {
          if (shorterLane == 0)
          {
            if (myCar.piecePosition.lane.startLaneIndex == 0)
              overtakeLane = 1;
            else if (myCar.piecePosition.lane.startLaneIndex == track.lanes.Count - 1)
              overtakeLane = -1;
            else
              overtakeLane = -1;
          }
          else
          {
            overtakeLane = 0;
          }
        }
        else
        {
          break;
        }
      }
    }

    if (overtakeLane != -100)
      return overtakeLane;

    return shorterLane;
  }

  private double calculateSpeed(Track track, CarPosition car, int currentTick, HistoryData history)
  {
    double distance = calculateDrivenDistance(track, car, history);
    double speed = distance / (currentTick - history.lastTick);

    return speed;
  }

  private double calculateDrivenDistance(Track track, CarPosition car, HistoryData history)
  {
    return calculateDistanceInRace(track, car.piecePosition.lap, car.piecePosition.pieceIndex, car.piecePosition.lane.startLaneIndex, car.piecePosition.inPieceDistance) -
      calculateDistanceInRace(track, history.lastLap, history.lastPiece, car.piecePosition.lane.startLaneIndex, history.lastPieceDistance);
  }

  private double calculateDistanceInRace(Track track, int lap, int pieceIndex, int laneIndex, double pieceDistance)
  {
    return lap * track.TrackLength(laneIndex) + track.TrackLengthToPiece(pieceIndex, laneIndex) + pieceDistance;
  }

  private double determineThrotle(Track track, CarPosition car, double currentSpeed, HistoryData history, int gameTick, int laps, double distanceToNearestCar, int @switch)
  {
    double throtle = 1;
    double AngleDifferingSpeed = (car.angle - history.lastAngle) / (gameTick - history.lastTick);
    double AngleDifferingAcceleration = (AngleDifferingSpeed - history.lastAngleSpeed) / (gameTick - history.lastTick);
    
    int indexOfSlowerPiece = track.indexOfNextSlowerPiece(car.piecePosition.pieceIndex, car.piecePosition.lane.startLaneIndex);
    int indexOfNextSwitch = track.indexOfNextSwitch(car.piecePosition.pieceIndex);
    int laneIndex = car.piecePosition.lane.startLaneIndex;
    if (indexOfNextSwitch < indexOfSlowerPiece || (car.piecePosition.pieceIndex > indexOfSlowerPiece && indexOfNextSwitch > indexOfSlowerPiece))
      laneIndex += @switch;

    if (track.pieces[car.piecePosition.pieceIndex].@switch)
      laneIndex = car.piecePosition.lane.endLaneIndex;

    double targetSpeedOfNextSlowerPiece = track.TargetSpeedOfNextSlowerPiece(car.piecePosition.pieceIndex, laneIndex);
    double distanceToSlowerPiece = track.DistanceToNextSlowerPiece(car.piecePosition.pieceIndex, car.piecePosition.inPieceDistance, car.piecePosition.lane.startLaneIndex);
    double brakeDistance = 0;
    double distanceToEndOfBend = track.DistanceToEndOfBend(car.piecePosition.pieceIndex, car.piecePosition.inPieceDistance, car.piecePosition.lane.startLaneIndex);

    double angleGuess = car.angle + AngleDifferingSpeed * distanceToEndOfBend / currentSpeed + AngleDifferingAcceleration * distanceToEndOfBend / currentSpeed * distanceToEndOfBend / currentSpeed / 2;


    if (history.lastLap != car.piecePosition.lap)
      history.SpeedAddedThisLap = 0;

    if (history.maxAngleGuess >= 0)
      history.maxAngleGuess = System.Math.Max(angleGuess, history.maxAngleGuess);

    if (history.maxAngleGuess <= 0)
      history.maxAngleGuess = System.Math.Min(angleGuess, history.maxAngleGuess);

    if (history.bendMaxAngle <= 0 && car.angle < 0)
    {
      history.bendMaxAngle = System.Math.Min(car.angle, history.bendMaxAngle);
    }
    else if (history.bendMaxAngle >= 0 && car.angle > 0)
    {
      history.bendMaxAngle = System.Math.Max(car.angle, history.bendMaxAngle);
    }
    else
    {
      if ((distanceToNearestCar <= 0 || distanceToNearestCar > 60) && history.hasSwithedDrift && history.SpeedAddedThisLap < 0.05 && System.Math.Abs(history.bendMaxAngle) > 0 && //10
        System.Math.Abs(history.bendMaxAngle) < 53 && System.Math.Abs(history.maxAngleGuess) < 90)
      {
        double additionalSpeed = (35 - System.Math.Abs(history.bendMaxAngle)) * (35 - System.Math.Abs(history.bendMaxAngle)) / 10000;//45
        additionalSpeed = System.Math.Min(additionalSpeed, System.Math.Min(history.SpeedAddedLast, history.SpeedRemovedLast * 2 / 3));/// 2));
        history.SpeedAddedLast = additionalSpeed;
        history.SpeedAddedThisLap += additionalSpeed;
        history.SpeedRemovedLast = history.SpeedRemovedLast * 1.2;
        track.SetForce(track.pieces[car.piecePosition.pieceIndex].targetSpeed + additionalSpeed);
        Console.WriteLine("accelerating " + track.pieces[car.piecePosition.pieceIndex].targetSpeed);
        history.hasAccelerated = true;
      }

      if (System.Math.Abs(history.bendMaxAngle) > 50 && System.Math.Abs(history.maxAngleGuess) > 200)//35
      {
        double slowerSpeed = (System.Math.Abs(history.bendMaxAngle) - 40) * (System.Math.Abs(history.bendMaxAngle) - 35) / 5000;
        slowerSpeed = System.Math.Min(slowerSpeed, history.SpeedAddedLast / 2);
        history.SpeedRemovedLast = slowerSpeed;
        track.SetForce(track.pieces[car.piecePosition.pieceIndex].targetSpeed - slowerSpeed);
        Console.WriteLine("slowing " + track.pieces[car.piecePosition.pieceIndex].targetSpeed);
      }

      if (System.Math.Abs(history.bendMaxAngle) > 55)
      {
        track.maxForce = track.pieces[car.piecePosition.pieceIndex].targetSpeed;
      }

      history.bendMaxAngle = 0;
      history.maxAngleGuess = 0;
      history.hasSwithedDrift = false;
    }

    int ticksToSpeed = (int)(2 * (targetSpeedOfNextSlowerPiece - currentSpeed) / (history.c * currentSpeed * currentSpeed) + 0.5);
    double distanceInTicks = currentSpeed * ticksToSpeed + 0.5 * history.c * ticksToSpeed * ticksToSpeed;
    //Console.WriteLine("ticks : " + ticksToSpeed);
    //Console.WriteLine("distance to brake: " + distanceInTicks);
    //Console.WriteLine("distance to slower: " + distanceToSlowerPiece);

    if (car.piecePosition.pieceIndex != history.lastPiece)
    {
      history.hasSwithedDrift = true;
      history.laneSwithced = false;

      Console.WriteLine("--------" + targetSpeedOfNextSlowerPiece + "-----------");
      Console.WriteLine(currentSpeed + " vs " + track.pieces[car.piecePosition.pieceIndex].GetTargetSpeed(track.lanes[car.piecePosition.lane.startLaneIndex].distanceFromCenter));
      if (track.pieces[car.piecePosition.pieceIndex].radius != 0)
        track.doingTrick = false;
    }


    if (distanceToSlowerPiece > 0 && targetSpeedOfNextSlowerPiece > 0 && currentSpeed > targetSpeedOfNextSlowerPiece)
    {
      brakeDistance = -1 * (currentSpeed - targetSpeedOfNextSlowerPiece) / history.brake * currentSpeed;
      //Console.WriteLine(brakeDistance + " vs " + distanceToSlowerPiece);
    }

    //Console.WriteLine("current " + currentSpeed);
    if (track.IsLastStraight(car.piecePosition.pieceIndex) && car.piecePosition.lap == laps - 1)
    {
      throtle = 1;
      Console.WriteLine("Throtle " + currentSpeed);

    }
    else if (brakeDistance != 0 && ((!history.turboActivated && distanceToSlowerPiece < distanceInTicks + currentSpeed * currentSpeed * currentSpeed / 10/*10*/) || (history.turboActivated && distanceToSlowerPiece < brakeDistance)) && 
      !track.doingTrick)
    {
      Console.WriteLine("Brake " + currentSpeed + " " + targetSpeedOfNextSlowerPiece + " " + distanceToSlowerPiece + " " + distanceInTicks);
      throtle = 0;
    }//bend pieces
    else if (track.pieces[car.piecePosition.pieceIndex].radius != 0 && currentSpeed > 2)
    {
      if (history.hasAccelerated && System.Math.Abs(car.angle) > 52)//49)
      {
        double slowerSpeed = (history.SpeedAddedLast / 13);
        history.SpeedRemovedLast = history.SpeedRemovedLast + slowerSpeed / 2;
        track.SetForce(track.pieces[car.piecePosition.pieceIndex].targetSpeed - slowerSpeed);

        Console.WriteLine("Brake in bend + angle " + car.angle);
        Console.WriteLine("slowing " + track.pieces[car.piecePosition.pieceIndex].targetSpeed);

        throtle = 0;
      }
      else if ((currentSpeed > track.pieces[car.piecePosition.pieceIndex].GetTargetSpeed(track.lanes[car.piecePosition.lane.startLaneIndex].distanceFromCenter)))
      {
        Console.WriteLine("Brake in bend " + currentSpeed);

        throtle = 0;
      }
      else
      {
        Console.WriteLine("Throtle in bend " + currentSpeed);
        throtle = 1;
      }

      if (throtle <= 0)
      {
        throtle = 0;
      }

      if (throtle > 1)
      {
        throtle = 1;
      }

      if (history.turboActivated)
        throtle = throtle / history.turboFactor;
    }
    else
    {
      Console.WriteLine("Throtle " + currentSpeed + " " + targetSpeedOfNextSlowerPiece);
    }



    history.lastAngleSpeed = AngleDifferingSpeed;
    return throtle;
  }

  private void send(SendMsg msg)
  {
    writer.WriteLine(msg.ToJson());
  }
}


class GameInfo
{
  public string gameId;
  public int gameTick;

  public GameInfo()
  {
  }
}

class LaneDecission
{
  public int currentLane;
  public int startIndex;
  public int endIndex;
  private double totalNumber;
  private double countOfObservations;
  private SortedList<int, double> speeds;
  public int startTick;
  public LaneDecission()
  {
    this.speeds = new SortedList<int, double>();
    this.startIndex = -1;
    this.endIndex = -1;
    this.totalNumber = 0;
    this.countOfObservations = 0;
    this.currentLane = 0;
    this.startTick = 0;
  }

  public LaneDecission(int startIndex, int endIndex)
  {
    this.speeds = new SortedList<int, double>();
    this.startIndex = startIndex;
    this.endIndex = endIndex;
    this.totalNumber = 0;
    this.countOfObservations = 0;
    this.currentLane = 0;
    this.startTick = 0;
  }

  public LaneDecission(LaneDecission oldOne)
  {
    this.speeds = new SortedList<int, double>();
    this.startIndex = oldOne.startIndex;
    this.endIndex = oldOne.endIndex;
    this.totalNumber = 0;
    this.countOfObservations = 0;
    this.currentLane = 0;
    this.startTick = 0;
  }

  public double getAverageSpeed()
  {
    double averageValue = 0;
    int count = 0;
    foreach (double value in this.speeds.Values)
    {
      if (3 < value)
      {
        averageValue += value;
        count++;
      }
    }

    if (this.speeds.Count != 0)
      return averageValue / this.speeds.Count;

    return 0;
  }

  public void addSpeedObservation(double speed)
  {
    this.speeds.Add(this.speeds.Count + 1, speed);
  }

  public double getAverageLane()
  {
    if (this.countOfObservations > 0)
      return this.totalNumber / this.countOfObservations;

    return 0;
  }

  public void addLaneObservation(int laneIndex)
  {
    this.currentLane = laneIndex;
    this.totalNumber += laneIndex;
    this.countOfObservations++;
  }
}


class HistoryData
{
  public double c;
  public SortedList<string, SortedList<int, LaneDecission>> laneDecissions;
  public int lastLane;
  public int lastLap;
  public int lastTick;
  public int lastPiece;
  public double lastSpeed;
  public double lastPieceDistance;
  public double lastThrotle;
  public double lastAngle;
  public double lastAngleSpeed;
  public double maxAngleGuess;
  public double SpeedAddedLast;
  public double SpeedRemovedLast;
  public double SpeedAddedThisLap;
  public double turboFactor;
  public bool turboAvailable;
  public bool turboActivated;
  public int turboTicksLeft;
  public bool hasSwithedDrift;
  public bool initialised;
  public bool laneSwithced;

  public double bendMaxAngle;
  public double brake;
  public double lastRaceTargetForce;
  public SortedList<double, CarPosition> lastDistances;
  public CarPosition carToDodge;
  public bool hasAccelerated;

  public HistoryData()
  {
    this.hasAccelerated = false;
    this.c = 0;
    this.carToDodge = null;
    this.lastDistances = new SortedList<double, CarPosition>();
    this.lastLane = 0;
    this.laneSwithced = false;
    this.laneDecissions = new SortedList<string, SortedList<int, LaneDecission>>();
    this.lastRaceTargetForce = 0.4;//this is the one to adjust
    this.initialised = false;
    this.hasSwithedDrift = false;
    this.turboActivated = false;
    this.turboAvailable = false;
    this.turboTicksLeft = 0;
    this.turboFactor = 1;
    this.SpeedAddedThisLap = 0;
    this.lastAngleSpeed = 0;
    this.lastPiece = 0;
    this.lastPieceDistance = 0;
    this.lastTick = 0;
    this.lastThrotle = 1;
    this.lastAngle = 0;
    this.brake = -0.082;
    this.lastSpeed = 0;
    this.bendMaxAngle = 0;
    this.maxAngleGuess = 0;
    this.SpeedAddedLast = 0.1;
    this.SpeedRemovedLast = 0.1;
  }


}

class Piece
{
  public double length;
  public bool @switch;
  public double radius;
  public double angle;
  public double targetSpeed;

  public Piece()
  {
    this.length = 0;
    this.radius = 0;
    this.angle = 0;
    this.@switch = false;
    this.targetSpeed = 0.4;
  }

  public void init()
  {
    if (radius > 0)
    {
      this.length = System.Math.Abs(angle) / 360.0 * 2.0 * System.Math.PI * this.radius;
    }
  }

  public double GetTargetSpeed(double differenceInRadius)
  {
    if (this.angle > 0)
      differenceInRadius = -1 * differenceInRadius;

    if (radius > 0)
    {
      return System.Math.Sqrt(this.targetSpeed * (this.radius + differenceInRadius));//6.5 m*V2/r
    }

    return 100;
  }

  public double GetLength(double differenceInRadius)
  {
    if (this.angle > 0)
      differenceInRadius = -1 * differenceInRadius;

    if (radius > 0)
    {
      return System.Math.Abs(angle) / 360.0 * 2.0 * System.Math.PI * (this.radius + differenceInRadius);
    }

    return this.length;
  }
}

class RaceInit
{
  public Race race;

  public RaceInit(Race race)
  {
    this.race = race;
  }
}

class Race
{
  public Track track;
  public System.Collections.Generic.List<Car> cars;
  public Session raceSession;

  public Race(Track track, System.Collections.Generic.List<Car> cars,
    Session raceSession)
  {
    this.track = track;
    this.cars = cars;
    this.raceSession = raceSession;
  }

  public void init()
  {
    this.track.init();
  }
}

class Session
{
  public int laps;
  public int maxLapTimeMs;
  public bool quickRace;

  public Session(int laps, int maxLapTimeMs, bool quickRace)
  {
    this.laps = laps;
    this.maxLapTimeMs = maxLapTimeMs;
    this.quickRace = quickRace;
  }
}

class Track
{
  public string id;
  public string name;
  public System.Collections.Generic.List<Piece> pieces;
  public System.Collections.Generic.List<Lane> lanes;
  public Point startingPoint;
  public SortedList<string, int> lastProcessedSwitch;
  public bool doingTrick;
  public double maxForce;

  public Track(string id, string name, System.Collections.Generic.List<Piece> pieces,
    System.Collections.Generic.List<Lane> lanes, Point startingPoint)
  {
    this.maxForce = 1.0;
    this.doingTrick = false;
    this.id = id;
    this.name = name;
    this.pieces = pieces;
    this.lanes = lanes;
    this.startingPoint = startingPoint;
    this.lastProcessedSwitch = new SortedList<string, int>();
  }

  public void SetForce(double force)
  {
    force = System.Math.Min(force, this.maxForce);
    foreach (Piece piece in this.pieces)
    {
      piece.targetSpeed = force;
    }
  }

  public double TrackLength(int laneIndex)
  {
    double length = 0;
    foreach (Piece piece in this.pieces)
    {
      length += piece.GetLength(this.lanes[laneIndex].distanceFromCenter);
    }

    return length;
  }

  public SortedList<int, LaneDecission> GetLaneDecissions()
  {
    SortedList<int, LaneDecission> lanes = new SortedList<int, LaneDecission>();

    int lastSwitch = 0;
    LaneDecission firstDecission = new LaneDecission();
    int index = 0;
    while (index < pieces.Count)
    {
      if (this.pieces[index].@switch)
      {
        if (lastSwitch == 0)
        {
          firstDecission.endIndex = index;
        }
        else
        {
          lanes.Add(lastSwitch, new LaneDecission(lastSwitch, index));
        }

        lastSwitch = index;
      }
      index++;
    }

    firstDecission.startIndex = lastSwitch;

    lanes.Add(lastSwitch, firstDecission);

    return lanes;
  }

  public double TrackLengthToPiece(int pieceIndex, int laneIndex)
  {
    double length = 0;
    int index = 0;
    while (index < pieceIndex)
    {
      length += this.pieces[index].GetLength(this.lanes[laneIndex].distanceFromCenter);
      index++;
    }
    return length;
  }

  public void init()
  {
    foreach (Piece piece in this.pieces)
    {
      piece.init();
    }
  }

  public double TargetSpeedOfNextSlowerPiece(int pieceIndex, int laneIndex)
  {
    double speed = 0;
    int slowerPieceIndex = this.indexOfNextSlowerPiece(pieceIndex, laneIndex);

    if (slowerPieceIndex != -1)
    {
      int indexOfEndOfBend = this.indexOfEndOfBend(slowerPieceIndex);

      speed = this.pieces[slowerPieceIndex].GetTargetSpeed(this.lanes[laneIndex].distanceFromCenter);
    }

    return speed;
  }

  public double DistanceToEndOfBend(int fromPieceIndex, double inPieceDistance, int laneIndex)
  {
    double distance = 0;
    int indexOfEnd = this.indexOfEndOfBend(fromPieceIndex);
    if (indexOfEnd != -1)
    {
      if (indexOfEnd < fromPieceIndex)
      {
        distance = this.TrackLength(laneIndex) - (this.TrackLengthToPiece(fromPieceIndex, laneIndex) + inPieceDistance) + this.TrackLengthToPiece(indexOfEnd, laneIndex);
      }
      else
      {
        distance = this.TrackLengthToPiece(indexOfEnd, laneIndex) - (this.TrackLengthToPiece(fromPieceIndex, laneIndex) + inPieceDistance);
      }
    }
    return distance;
  }

  public int indexOfEndOfBend(int pieceIndex)
  {
    int index = pieceIndex;
    while (index < pieces.Count)
    {
      if (this.pieces[index].radius == 0 || (this.pieces[pieceIndex].angle < 0 && this.pieces[index].angle > 0) ||
        (this.pieces[pieceIndex].angle > 0 && this.pieces[index].angle < 0))
      {
        return index;
      }
      index++;
    }

    index = 0;
    while (index < pieceIndex)
    {
      if (this.pieces[index].radius == 0 || (this.pieces[pieceIndex].angle < 0 && this.pieces[index].angle > 0) ||
        (this.pieces[pieceIndex].angle > 0 && this.pieces[index].angle < 0))
      {
        return index;
      }
      index++;
    }

    return -1;
  }
  public int indexOfEndOfStraight(int pieceIndex)
  {
    int index = pieceIndex;
    while (index < pieces.Count)
    {
      if (this.pieces[index].radius != 0)
      {
        return index;
      }
      index++;
    }

    index = 0;
    while (index < pieceIndex)
    {
      if (this.pieces[index].radius != 0)
      {
        return index;
      }
      index++;
    }

    return -1;
  }
  public double DistanceToEndOfStraight(int fromPieceIndex, double inPieceDistance, int laneIndex)
  {
    double distance = 0;
    int indexOfEnd = this.indexOfEndOfStraight(fromPieceIndex);
    if (indexOfEnd != -1)
    {
      if (indexOfEnd < fromPieceIndex)
      {
        distance = this.TrackLength(laneIndex) - (this.TrackLengthToPiece(fromPieceIndex, laneIndex) + inPieceDistance) + this.TrackLengthToPiece(indexOfEnd, laneIndex);
      }
      else
      {
        distance = this.TrackLengthToPiece(indexOfEnd, laneIndex) - (this.TrackLengthToPiece(fromPieceIndex, laneIndex) + inPieceDistance);
      }
    }
    return distance;
  }

  public double DistanceToNextSlowerPiece(int fromPieceIndex, double inPieceDistance, int laneIndex)
  {
    double distance = 0;
    int indexOfSlowerPiece = indexOfNextSlowerPiece(fromPieceIndex, laneIndex);
    if (indexOfSlowerPiece != -1)
    {
      if (indexOfSlowerPiece < fromPieceIndex)
      {
        distance = this.TrackLength(laneIndex) - (this.TrackLengthToPiece(fromPieceIndex, laneIndex) + inPieceDistance) + this.TrackLengthToPiece(indexOfSlowerPiece, laneIndex);
      }
      else
      {
        distance = this.TrackLengthToPiece(indexOfSlowerPiece, laneIndex) - (this.TrackLengthToPiece(fromPieceIndex, laneIndex) + inPieceDistance);
      }
    }
    return distance;
  }

  public double DistanceToPiece(int fromPieceIndex, int toPieceIndex, int laneIndex)
  {
    double distance = 0;

    if (toPieceIndex < fromPieceIndex)
    {
      distance = this.TrackLength(laneIndex) - this.TrackLengthToPiece(fromPieceIndex, laneIndex) + this.TrackLengthToPiece(toPieceIndex, laneIndex);
    }
    else
    {
      distance = this.TrackLengthToPiece(toPieceIndex, laneIndex) - (this.TrackLengthToPiece(fromPieceIndex, laneIndex));
    }

    return distance;
  }


  public int indexOfNextSlowerPiece(int pieceIndex, int laneIndex)
  {
    int index = pieceIndex;
    while (index < pieces.Count)
    {
      if (this.pieces[index].GetTargetSpeed(this.lanes[laneIndex].distanceFromCenter) <
        this.pieces[pieceIndex].GetTargetSpeed(this.lanes[laneIndex].distanceFromCenter))
      {
        return index;
      }
      index++;
    }

    index = 0;
    while (index < pieceIndex)
    {
      if (this.pieces[index].GetTargetSpeed(this.lanes[laneIndex].distanceFromCenter) <
        this.pieces[pieceIndex].GetTargetSpeed(this.lanes[laneIndex].distanceFromCenter))
      {
        return index;
      }
      index++;
    }

    return -1;
  }

  public int indexOfPreviousSwitch(int pieceIndex)
  {
    int index = pieceIndex - 1;
    while (index >= 0)
    {
      if (this.pieces[index].@switch)
        return index;
      index--;
    }

    index = this.pieces.Count - 1;
    while (index >= pieceIndex)
    {
      if (this.pieces[index].@switch)
        return index;
      index--;
    }

    return 0;
  }

  public int indexOfNextSwitch(int pieceIndex)
  {
    int index = pieceIndex + 1;
    while (index < this.pieces.Count)
    {
      if (this.pieces[index].@switch)
        return index;
      index++;
    }

    index = 0;
    while (index < pieceIndex)
    {
      if (this.pieces[index].@switch)
        return index;
      index++;
    }

    return 0;
  }

  public int getShorterLane(int pieceIndex, int laneIndex)
  {
    int indexOfNextSwitch = pieceIndex;
    while (indexOfNextSwitch < this.pieces.Count)
    {
      if (this.pieces[indexOfNextSwitch].@switch)
      {
        break;
      }
      indexOfNextSwitch++;
    }

    if (indexOfNextSwitch == this.pieces.Count)
    {
      indexOfNextSwitch = 0;
      while (indexOfNextSwitch <= pieceIndex)
      {
        if (this.pieces[indexOfNextSwitch].@switch)
        {
          break;
        }
        indexOfNextSwitch++;
      }

      if (indexOfNextSwitch > pieceIndex)
        return 0;
    }


    int indexOfSwitchAfterNext = indexOfNextSwitch + 1;
    while (indexOfSwitchAfterNext < this.pieces.Count)
    {
      if (this.pieces[indexOfSwitchAfterNext].@switch)
      {
        break;
      }
      indexOfSwitchAfterNext++;
    }

    if (indexOfSwitchAfterNext == this.pieces.Count)
    {
      indexOfSwitchAfterNext = 0;
      while (indexOfSwitchAfterNext <= pieceIndex)
      {
        if (this.pieces[indexOfSwitchAfterNext].@switch)
        {
          break;
        }
        indexOfSwitchAfterNext++;
      }

      if (indexOfSwitchAfterNext > pieceIndex)
        return 0;
    }


    double lengthOfLeftOne = 0;
    double lengthOfRightOne = 0;
    double lengthOfOne = 0;


    if (laneIndex == 0)
    {
      lengthOfOne = DistanceToPiece(indexOfNextSwitch, indexOfSwitchAfterNext, laneIndex);
      lengthOfRightOne = DistanceToPiece(indexOfNextSwitch, indexOfSwitchAfterNext, laneIndex + 1);

      if (lengthOfRightOne < lengthOfOne)
        return 1;
      else
        return 0;
    }
    else if (laneIndex < this.lanes.Count - 1)
    {
      lengthOfOne = DistanceToPiece(indexOfNextSwitch, indexOfSwitchAfterNext, laneIndex);
      lengthOfLeftOne = DistanceToPiece(indexOfNextSwitch, indexOfSwitchAfterNext, laneIndex - 1);
      lengthOfRightOne = DistanceToPiece(indexOfNextSwitch, indexOfSwitchAfterNext, laneIndex + 1);

      if (lengthOfLeftOne < lengthOfOne && lengthOfLeftOne < lengthOfRightOne)
        return -1;
      else if (lengthOfRightOne < lengthOfOne && lengthOfRightOne < lengthOfLeftOne)
        return 1;
      else
        return 0;
    }
    else if (laneIndex == this.lanes.Count - 1)
    {
      lengthOfOne = DistanceToPiece(indexOfNextSwitch, indexOfSwitchAfterNext, laneIndex);
      lengthOfLeftOne = DistanceToPiece(indexOfNextSwitch, indexOfSwitchAfterNext, laneIndex - 1);
      if (lengthOfLeftOne <= lengthOfOne)
        return -1;
      else
        return 0;
    }

    return 0;
  }

  public bool LetsTurbo(int pieceIndex, int turboTicksLeft, CarPosition myCar, CarPosition nearestCar, int laps, double distanceToNearest)
  {
    if (this.pieces[pieceIndex].radius == 0)
    {
      int indexOfNextBend = pieceIndex;
      while (indexOfNextBend < this.pieces.Count)
      {
        if (this.pieces[indexOfNextBend].radius != 0)
        {
          break;
        }
        indexOfNextBend++;
      }

      if (indexOfNextBend == this.pieces.Count)
      {
        indexOfNextBend = 0;
        while (indexOfNextBend <= pieceIndex)
        {
          if (this.pieces[indexOfNextBend].radius != 0)
          {
            break;
          }
          indexOfNextBend++;
        }

        if (indexOfNextBend > pieceIndex)
          return false;
      }
      bool enoughLongStraight = (this.IsLastStraight(myCar.piecePosition.pieceIndex) && myCar.piecePosition.lap == laps - 1) ||
         ((distanceToNearest > 120 || distanceToNearest <= 0) && DistanceToPiece(pieceIndex, indexOfNextBend, 0) > turboTicksLeft * 10);

      bool shouldTurboBeUsed = enoughLongStraight;

      if (nearestCar != null && this.pieces[myCar.piecePosition.lane.startLaneIndex].radius == 0 && this.pieces[myCar.piecePosition.lane.startLaneIndex].radius == 0 &&
        !this.pieces[myCar.piecePosition.lane.startLaneIndex].@switch && !this.pieces[myCar.piecePosition.lane.startLaneIndex].@switch)
      {
        bool sameStraight = false;
        int index = myCar.piecePosition.pieceIndex;
        while (index < this.pieces.Count)
        {
          if (index == nearestCar.piecePosition.pieceIndex)
            sameStraight = true;

          if (this.pieces[index].radius != 0)
          {
            break;
          }
          index++;
        }

        double myDistanceToBend = this.DistanceToEndOfStraight(myCar.piecePosition.pieceIndex, myCar.piecePosition.inPieceDistance, myCar.piecePosition.lane.startLaneIndex);
        double otherDistanceToBend = this.DistanceToEndOfStraight(nearestCar.piecePosition.pieceIndex, nearestCar.piecePosition.inPieceDistance, nearestCar.piecePosition.lane.startLaneIndex);
        if (sameStraight && myDistanceToBend != 0 && otherDistanceToBend != 0)
        {
          if (myDistanceToBend > 180 && myDistanceToBend < 300 && myDistanceToBend - otherDistanceToBend < 110 && myDistanceToBend - otherDistanceToBend > 60 &&
            myCar.piecePosition.lane.startLaneIndex == nearestCar.piecePosition.lane.startLaneIndex)
          {
            double distanceToNextSwitch = this.DistanceToPiece(myCar.piecePosition.pieceIndex, this.indexOfNextSwitch(myCar.piecePosition.pieceIndex), myCar.piecePosition.lane.startLaneIndex);
            if (distanceToNextSwitch > 100 && this.TargetSpeedOfNextSlowerPiece(nearestCar.piecePosition.pieceIndex, nearestCar.piecePosition.lane.endLaneIndex) < 7)
            {
              Console.WriteLine(myCar.piecePosition.pieceIndex + " " + nearestCar.piecePosition.pieceIndex);
              shouldTurboBeUsed = true;
              this.doingTrick = true;
            }
          }
        }
      }

      return shouldTurboBeUsed;
    }

    return false;
  }

  public bool IsLastStraight(int pieceIndex)
  {
    if (this.pieces[pieceIndex].radius == 0)
    {
      int indexOfNextBend = pieceIndex;
      while (indexOfNextBend < this.pieces.Count)
      {
        if (this.pieces[indexOfNextBend].radius != 0)
        {
          break;
        }
        indexOfNextBend++;
      }

      return indexOfNextBend == this.pieces.Count;
    }

    return false;
  }
}

class Dimensions
{
  public double length;
  public double width;
  public double guideFlagPosition;

  public Dimensions(double length, double width, double guideFlagPosition)
  {
    this.length = length;
    this.width = width;
    this.guideFlagPosition = guideFlagPosition;
  }
}

class Point
{
  public Position position;
  public double angle;

  public Point(Position position, double angle)
  {
    this.position = position;
    this.angle = angle;
  }
}


class Lane
{
  public int distanceFromCenter;
  public int index;

  public Lane(int distanceFromCenter, int index)
  {
    this.distanceFromCenter = distanceFromCenter;
    this.index = index;
  }
}

class MsgWrapper
{
  public string msgType;
  public Object data;

  public MsgWrapper(string msgType, Object data)
  {
    this.msgType = msgType;
    this.data = data;
  }
}

abstract class SendMsg
{
  public string ToJson()
  {
    return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
  }
  protected virtual Object MsgData()
  {
    return this;
  }

  protected abstract string MsgType();
}

class Join : SendMsg
{
  public string name;
  public string key;

  public Join(string name, string key)
  {
    this.name = name;
    this.key = key;
  }

  protected override string MsgType()
  {
    return "join";
  }
}

class Ping : SendMsg
{
  protected override string MsgType()
  {
    return "ping";
  }
}

class JoinRace : SendMsg
{
  public string trackName;
  public string password;
  public int carCount;
  public Join botId;

  public JoinRace(string trackName, string password, int carCount, Join botId)
  {
    this.trackName = trackName;
    this.password = password;
    this.carCount = carCount;
    this.botId = botId;
  }

  protected override string MsgType()
  {
    return "joinRace";
  }
}

class CarPosition
{
  public CarIdentifier id;
  public double angle;

  public Position piecePosition;

  public CarPosition(CarIdentifier id, double angle, Position piecePosition)
  {
    this.id = id;
    this.angle = angle;
    this.piecePosition = piecePosition;
  }
}


class Car
{
  public CarIdentifier id;
  public Dimensions dimensions;
  public Car(CarIdentifier id, Dimensions dimensions)
  {
    this.id = id;
    this.dimensions = dimensions;
  }
}

class Position
{
  public int pieceIndex;
  public double inPieceDistance;
  public CarLane lane;
  public int lap;

  public Position(int pieceIndex, double inPieceDistance, CarLane lane, int lap)
  {
    this.pieceIndex = pieceIndex;
    this.inPieceDistance = inPieceDistance;
    this.lane = lane;
    this.lap = lap;
  }
}

class Turbo
{
  public double turboDurationMilliseconds;
  public int turboDurationTicks;
  public double turboFactor;

  public Turbo(double turboDurationMilliseconds, int turboDurationTicks, double turboFactor)
  {
    this.turboDurationMilliseconds = turboDurationMilliseconds;
    this.turboDurationTicks = turboDurationTicks;
    this.turboFactor = turboFactor;
  }
}

class CarLane
{
  public int startLaneIndex;
  public int endLaneIndex;

  public CarLane(int startLaneIndex, int endLaneIndex)
  {
    this.startLaneIndex = startLaneIndex;
    this.endLaneIndex = endLaneIndex;
  }
}

class CarIdentifier : SendMsg
{
  public string name;
  public string color;

  public CarIdentifier(string name, string color)
  {
    this.name = name;
    this.color = color;
  }

  protected override string MsgType()
  {
    return "yourCar";
  }
}
class SwitchLane : SendMsg
{
  public string value;
  public int gameTick;

  public SwitchLane(string value, int gameTick)
  {
    this.value = value;
    this.gameTick = gameTick;
  }

  protected override Object MsgData()
  {
    return this.value;
  }

  protected override string MsgType()
  {
    return "switchLane";
  }
}

class TurboActivate : SendMsg
{
  public string value;
  public int gameTick;
  public TurboActivate(string value, int gameTick)
  {
    this.value = value;
    this.gameTick = gameTick;
  }

  protected override Object MsgData()
  {
    return this.value;
  }

  protected override string MsgType()
  {
    return "turbo";
  }
}

class Throttle : SendMsg
{
  public double value;
  public int gameTick;

  public Throttle(double value, int gameTick)
  {
    this.value = value;
    this.gameTick = gameTick;
  }

  protected override Object MsgData()
  {
    return this.value;
  }

  protected override string MsgType()
  {
    return "throttle";
  }
}